from days import aoc, day
from itertools import chain, permutations
import re

day_number = 13


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        p = re.compile(r'(\w*) .* (gain|lose) (\d+) .* (\w*)\.')
        self.raw_weights = {(m.group(1), m.group(4)):
                            int(m.group(3))
                            * (1 if m.group(2) == "gain" else -1)
                            for m in [p.match(l) for l in input_data]}
        self.people = set(chain(*self.raw_weights))
        self.weights = {c: self.raw_weights[c] + self.raw_weights[c[::-1]]
                        for c in self.raw_weights}

    def part1(self, input_data):
        return get_seating_and_happiness(
                    self.people,
                    self.weights,
                    lambda x, y: x > y)

    def part2(self, input_data):
        me = 'Me'
        people = *self.people, me

        weights = {c: 0 for c in permutations(people, 2)}
        weights.update(self.weights)

        return get_seating_and_happiness(
                    people,
                    weights,
                    lambda x, y: x > y)


def get_seating_and_happiness(people: list, weights: dict, diff) \
        -> "tuple(list(str), int)":
    happiness = None
    path = ''

    for p in permutations(people, len(people)):
        new_happiness = 0

        for i, c in enumerate(p):
            if i + 1 < len(p):
                new_happiness += weights[(c, p[i + 1])]
            else:
                new_happiness += weights[(c, p[0])]

        if not(happiness) or diff(new_happiness, happiness):
            happiness = new_happiness
            path = p

    return path, happiness


if __name__ == "__main__":
    DaySolution(day_number).run()
