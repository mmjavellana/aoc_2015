from days import aoc, day
from utils.decorators import time_it
from collections import namedtuple

day_number = 18


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        point = namedtuple('point', 'x y')
        self.lights = [int(v == '#') for i in input_data for v in i]
        self.bounds = point(len(input_data[0]), len(input_data))
        self.neighbors = {i: adjacent(i, *self.bounds)
                          for i, _ in enumerate(self.lights)}

    @time_it
    def part1(self, input_data):
        lights = self.lights[:]

        for _ in range(100):
            lights = [has_light(v, sum(lights[i] for i in self.neighbors[c]))
                      for c, v in enumerate(lights)]

        return sum(lights)

    @time_it
    def part2(self, input_data):
        lights = self.lights[:]

        for _ in range(100):
            lights = [has_light(v, sum(lights[i] for i in self.neighbors[c]))
                      for c, v in enumerate(lights)]

            lights[0] = lights[-1] = 1
            lights[self.bounds.x - 1] = 1
            lights[len(lights) - self.bounds[0]] = 1

        return sum(lights)


def has_light(on, adj):
    return int(adj == 3 or on and adj == 2)


def adjacent(point, width, height):
    adj = [p for n in (1, -1) if (p := point+n)//width == point//width]
    adj += [p 
            for m in (1, 0, -1)
            for n in (width, -width)
            if (0 <= (p := point+n+m) < width*height
                and abs(point//width - p//width) == 1)]
    return adj


if __name__ == "__main__":
    DaySolution(day_number).run()
