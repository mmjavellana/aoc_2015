from days import aoc, day
from utils.decorators import time_it
import re

day_number = 25


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.coordinates = [int(i) for i in re.findall(r'\d+', input_data[0])]

    @time_it
    def part1(self, input_data):
        y, x = self.coordinates
        val = sum(range(1, x + 1)) + sum(range(x, x + y - 1))
        start = 20151125
        mult = 252533
        mod = 33554393

        return start * pow(mult, val - 1, mod) % mod

    @time_it
    def part2(self, input_data):
        pass


if __name__ == "__main__":
    DaySolution(day_number).run()
