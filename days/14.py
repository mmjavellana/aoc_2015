from days import aoc, day
import re

day_number = 14


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.reindeer = {r[0]: [int(i) for i in re.findall(r'\d+', r[1])]
                         for r in (l.split(' ', maxsplit=1)
                                   for l in input_data)}

    def part1(self, input_data):
        return max(self.get_distances(2503).items(), key=lambda x: x[1])

    def part2(self, input_data):
        points = {name: 0 for name in self.reindeer}

        for time in range(2503):
            distances = self.get_distances(time + 1)
            points[max(distances, key=lambda x: distances[x])] += 1

        return max(points.items(), key=lambda x: x[1])

    def get_distances(self, time: int):
        return {name: (cycles * fly + min(remainder, fly)) * speed
                for name, (speed, fly, rest), (cycles, remainder) in
                [(k, v, divmod(time, v[1] + v[2]))
                for k, v in self.reindeer.items()]}


if __name__ == "__main__":
    DaySolution(day_number).run()
