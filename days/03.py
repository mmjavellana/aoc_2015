from days import aoc, day
from collections import defaultdict

day_number = 3


def directions(argument):
    switcher = {
        ">": (1, 0),
        "^": (0, 1),
        "<": (-1, 0),
        "v": (0, -1)
    }

    return switcher.get(argument, (0, 0))


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.data = str.join('', input_data)

    def part1(self, input_data):
        (x, y) = (0, 0)

        houses = defaultdict(lambda: defaultdict(int))

        houses[x][y] += 1

        for d in self.data:
            (x, y) = tuple(a + b for a, b in zip((x, y), directions(d)))
            houses[x][y] += 1

        total = sum(1 for s in houses.values() for i in s.values() if i > 0)

        return total

    def part2(self, input_data):
        (x, y) = (0, 0)
        (rx, ry) = (0, 0)

        houses = defaultdict(lambda: defaultdict(int))

        houses[x][y] += 1

        for (d, rd) in zip(*[iter(self.data)]*2):
            (x, y) = tuple(a + b for a, b in zip((x, y), directions(d)))
            (rx, ry) = tuple(a + b for a, b in zip((rx, ry), directions(rd)))

            houses[x][y] += 1
            houses[rx][ry] += 1

        total = sum(1 for s in houses.values() for i in s.values() if i > 0)

        return total


if __name__ == "__main__":
    DaySolution(day_number).run()