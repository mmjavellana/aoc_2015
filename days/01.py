from days import aoc, day

day_number = 1


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.data = str.join('', input_data)
        pass

    def part1(self, input_data):
        floor = 0

        for x in self.data:
            floor += 1 if x == "(" else -1

        return floor

    def part2(self, input_data):
        floor = 0

        for i, x in enumerate(self.data):
            floor += 1 if x == "(" else -1

            if floor < 0:
                return i + 1


if __name__ == "__main__":
    DaySolution(day_number).run()
