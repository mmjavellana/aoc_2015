from days import aoc, day
from itertools import chain, permutations

day_number = 9


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.weights = {(l, r): int(d) for l, _, r, _, d in
                        [l.split() for l in input_data]}
        self.weights.update({(r, l): int(d) for l, _, r, _, d in
                             [l.split() for l in input_data]})
        self.locations = set(chain(*self.weights))

    def part1(self, input_data):
        return get_route_and_distance(
                    self.locations,
                    self.weights,
                    lambda x, y: x < y
                    )

    def part2(self, input_data):
        return get_route_and_distance(
                    self.locations,
                    self.weights,
                    lambda x, y: x > y
                    )


def get_route_and_distance(locations: list, weights: dict, diff) \
        -> "tuple(list(str), int)":
    distance = None
    path = ''

    for p in permutations(locations, len(locations)):
        new_distance = 0

        for i, c in enumerate(p):
            if i + 1 < len(p):
                new_distance += weights[(c, p[i + 1])]

        if not(distance) or diff(new_distance, distance):
            distance = new_distance
            path = p

    return path, distance


if __name__ == "__main__":
    DaySolution(day_number).run()
