from days import aoc, day
import json
import re

day_number = 12


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.data = json.loads(input_data[0])

    def part1(self, input_data):
        return sum(int(i) for i in re.findall(r"-?\d+", input_data[0]))

    def part2(self, input_data):
        return sum_non_red_obj(self.data)


def sum_non_red_obj(obj: any):
    if isinstance(obj, int):
        return obj

    if isinstance(obj, list):
        return sum(sum_non_red_obj(l) for l in obj)

    if isinstance(obj, dict) and "red" not in obj.values():
        return sum_non_red_obj(list(obj.values()))

    return 0


if __name__ == "__main__":
    DaySolution(day_number).run()
