from days import aoc, day

day_number = 5


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        pass

    def part1(self, input_data):
        vowels = ['a', 'e', 'i', 'o', 'u']
        naughty = ['ab', 'cd', 'pq', 'xy']

        nice = 0

        for x in input_data:
            if any(ext in x for ext in naughty):
                continue

            if sum(x.count(c) for c in vowels) < 3:
                continue

            if sum(1 for (a, b) in zip(x, x[1:]) if a == b) < 1:
                continue

            nice += 1

        return nice

    def part2(self, input_data):
        nice = 0

        for x in input_data:
            if sum(1 for i, (a, b) in enumerate(zip(x, x[1:])) 
                    if a+b in x[i+2:]) < 1:
                continue

            if sum(1 for (a, b, c) in zip(x, x[1:], x[2:]) if a == c) < 1:
                continue

            nice += 1

        return nice


if __name__ == "__main__":
    DaySolution(day_number).run()