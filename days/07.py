from days import aoc, day
from collections import defaultdict
import re

day_number = 7


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.parsed_data = defaultdict(object)

        p = re.compile(r'(?P<a>[a-z0-9]+)?\s?(?P<b>[A-Z]+)?'
                       r'\s?(?P<c>\w+)?\s->\s(?P<d>\w+)?')

        def getStrOrInt(s: str):
            return int(s) if s and s.isnumeric() else s

        for i in input_data:
            m = p.match(i)
            self.parsed_data[m.group('d')] = (
                getStrOrInt(m.group('a')), 
                bit_fns(m.group('b')), 
                getStrOrInt(m.group('c'))
                )

    def part1(self, input_data):

        wires = traceWires(self.parsed_data, s='a')
        return wires['a']

    def part2(self, input_data):
        wires = traceWires(self.parsed_data, s='a')
        wires = traceWires(self.parsed_data, cache={'b': wires['a']}, s='a')

        return wires['a']


def bit_fns(argument):
    switcher = {
        "AND": lambda x, y: x & y,
        "OR": lambda x, y: x | y,
        "LSHIFT": lambda x, y: x << y,
        "RSHIFT": lambda x, y: x >> y,
        "NOT": lambda _, x: ~ x & 0xFFFF,
    }

    return switcher.get(argument, lambda x, _: x)


def traceWires(data, cache={}, s: str = None) -> "dict(int)":
    s = list(data)[0] if not(s) else s

    wires = defaultdict(int, cache)

    if s in wires:
        return wires

    d = data[s]

    def getValue(o, data, wires):
        if o and not(isinstance(o, int)):
            wires = traceWires(data, wires, o)
            o = wires[o]
        return (o, wires)

    l, wires = getValue(d[0], data, wires)
    r, wires = getValue(d[2], data, wires)

    wires[s] = d[1](l, r)

    return wires


if __name__ == "__main__":
    DaySolution(day_number).run()