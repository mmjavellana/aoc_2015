from days import aoc, day
from hashlib import md5

day_number = 4


def findHashStartsWith(secret, pattern):
    n = 0
    h = ""

    while not (h.startswith(pattern)):
        n += 1
        h = md5((secret + str(n)).encode('utf-8')).hexdigest()

    return n


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.secret = input_data[0]

    def part1(self, input_data):
        return findHashStartsWith(self.secret, "00000")

    def part2(self, input_data):
        return findHashStartsWith(self.secret, "000000")


if __name__ == "__main__":
    DaySolution(day_number).run()