from days import aoc, day
from utils.decorators import time_it
from itertools import combinations_with_replacement
from math import prod
import re

day_number = 15


@day(day_number)
class DaySolution(aoc):
    @time_it
    def common(self, input_data):
        ingredients = [tuple(map(int, re.findall(r'-?\d+', i)))
                       for i in input_data]
        recipes = (tuple(i.count(n) for n in set(i))
                   for i in combinations_with_replacement(
                        range(len(ingredients)), 100))
        self.scores = {p: calc_score(ingredients, p) for p in recipes}

    @time_it
    def part1(self, input_data):
        max_ = max(score for score, _ in self.scores.values())
        return max_, [k for k, (s, _) in self.scores.items() if s == max_]

    @time_it
    def part2(self, input_data):
        scores = {k: score for k, (score, cals) in self.scores.items()
                  if cals == 500}
        max_ = max(scores.values())
        return max_, [k for k, v in scores.items() if v == max_]


def calc_score(ingredients: list[tuple[int]], counts: list[int]):
    scores = [[prop * c for prop in i]
              for i, c in zip(ingredients, counts)]
    totals = list(map(sum, zip(*scores)))
    return prod(max(i, 0) for i in totals[:4]), totals[-1]


if __name__ == "__main__":
    DaySolution(day_number).run()
