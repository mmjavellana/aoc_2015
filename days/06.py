from days import aoc, day
from itertools import product
from collections import defaultdict
import re

day_number = 6


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.parsed_data = []

        p = re.compile(r"(turn on|turn off|toggle)\s(\d+),"
                       r"(\d+)\s.*\s(\d+),(\d+)")

        for s in input_data:
            m = p.match(s)
            self.parsed_data.append((
                m.group(1),
                (int(m.group(2)), int(m.group(3))),
                (int(m.group(4)), int(m.group(5))),
            ))

    def part1(self, input_data):
        def light_fns(argument):
            switcher = {
                "turn on": lambda x: 1,
                "turn off": lambda x: 0,
                "toggle": lambda x: x ^ 1
            }

            return switcher.get(argument, lambda x: 0)

        lights = ProcessLights(self.parsed_data, light_fns)

        return len([i for i in lights.values() if i])

    def part2(self, input_data):
        def light_fns(argument):
            switcher = {
                "turn on": lambda x: x + 1,
                "turn off": lambda x: max(x-1, 0),
                "toggle": lambda x: x + 2
            }

            return switcher.get(argument, lambda x: 0)

        lights = ProcessLights(self.parsed_data, light_fns)

        return sum([i for i in lights.values()])


def ProcessLights(data: tuple, light_fns) -> "dict(int)":
    lights = defaultdict(int)

    for i in data:
        light_fn = light_fns(i[0])

        for x, y in product(range(i[1][0], i[2][0] + 1), 
                            range(i[1][1], i[2][1] + 1)):
            lights[(x, y)] = light_fn(lights[(x, y)])

    return lights


if __name__ == "__main__":
    DaySolution(day_number).run()
