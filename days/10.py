from days import aoc, day
from itertools import groupby

day_number = 10


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.input_data = input_data[0]

    def part1(self, input_data):
        return len(step(self.input_data, 40))

    def part2(self, input_data):
        return len(step(self.input_data, 50))


def step(data: str, steps: int = 1) -> "str":
    for _ in range(steps):
        data = ''.join(str(len(list(g))) + i for i, g in groupby(data))

    return data


if __name__ == "__main__":
    DaySolution(day_number).run()
