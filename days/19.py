from days import aoc, day
from utils.decorators import time_it
import re

day_number = 19


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        replacements = [i.strip().split(' => ') for i in input_data[:-2]]
        self.molecule = input_data[-1]
        self.replacements = {k: [k] for k, _ in replacements}

        for k, r in replacements:
            self.replacements[k].append(r)

    @time_it
    def part1(self, input_data):
        molecules = {replace_at_index(self.molecule, k, r, m.start())
                     for k, rs in self.replacements.items()
                     for r in rs
                     for m in re.finditer(k, self.molecule)}

        return len(molecules)

    @time_it
    def part2(self, input_data):
        molecule = self.molecule
        return (sum(1 for m in molecule if m.isupper())
                - molecule.count('Ar')
                - molecule.count('Rn')
                - 2 * molecule.count('Y')
                - 1)


def replace_at_index(fullstr: str, old: str, new: str, pos: int):
    return fullstr[:pos] + new + fullstr[pos+len(old):]


if __name__ == "__main__":
    DaySolution(day_number).run()
