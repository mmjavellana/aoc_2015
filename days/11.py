from days import aoc, day
import re

day_number = 11


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.password = input_data[0]

    def part1(self, input_data):
        new_p = self.get_next_password(self.password)
        return self.password, new_p

    def part2(self, input_data):
        new_p = self.get_next_password(self.password)
        new_p = self.get_next_password(new_p)

        return self.password, new_p

    def get_next_password(self, password: str) -> "str":
        new_p = password
        bad_chars = ['i', 'o', 'l']
        p = re.compile(r".*(\w)\1.*(?!\1)(\w)\2.*")

        while True:
            new_p = increment_str(new_p)

            if any(c in new_p for c in bad_chars):
                continue

            if not(any(1 for (a, b, c) in zip(new_p, new_p[1:], new_p[2:])
                   if b == increment_char(a) and c == increment_char(b)
                   and 'z' not in a+b)):
                continue

            if not p.match(new_p):
                continue

            break
        return new_p


def increment_char(c: chr) -> "chr":
    return chr(ord(c.lower()) + 1) if c != 'z' else 'a'


def increment_str(s: str) -> "str":
    s = s.lower()
    lpart = s.rstrip('z')

    if not lpart:
        new_s = 'a' * (len(s) + 1)
    else:
        r = len(s) - len(lpart)
        new_s = lpart[:-1] + increment_char(lpart[-1])
        new_s += 'a' * r

    return new_s


if __name__ == "__main__":
    DaySolution(day_number).run()
