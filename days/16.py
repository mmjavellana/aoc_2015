from days import aoc, day
from utils.decorators import time_it

day_number = 16


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.sues = {s: {a: int(c)
                         for a, c in [attr.split(': ')
                                      for attr in v.split(', ')]}
                     for s, v in [i.split(': ', 1) for i in input_data]}
        self.  gift = {'children': 3,
                       'cats': 7,
                       'samoyeds': 2,
                       'pomeranians': 3,
                       'akitas': 0,
                       'vizslas': 0,
                       'goldfish': 5,
                       'trees': 3,
                       'cars': 2,
                       'perfumes': 1}

    @time_it
    def part1(self, input_data):
        sues = self.sues.copy()
        for attr, count in self.gift.items():
            sues = {k: v for k, v in sues.items()
                    if attr not in v or v[attr] == count}

        return sues

    @time_it
    def part2(self, input_data):
        def compare(key, attributes, target):
            if key in ('cats', 'trees'):
                return attributes[key] > target
            elif key in ('goldfish', 'pomeranians'):
                return attributes[key] < target
            else:
                return attributes[key] == target

        sues = self.sues.copy()
        for attr, count in self.gift.items():
            sues = {k: v for k, v in sues.items()
                    if attr not in v or compare(attr, v, count)}

        return sues


if __name__ == "__main__":
    DaySolution(day_number).run()
