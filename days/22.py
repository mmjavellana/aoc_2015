from days import aoc, day
from utils.decorators import time_it
from dataclasses import dataclass

day_number = 22


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.boss_hp, self.boss_dmg = (int(i.split()[-1])
                                       for i in input_data)
        self.player_hp, self.player_mana = (50, 500)

        self.spells = [
            spell('Magic Missle', 53, damage=4),
            spell('Drain', 73, damage=2, health=2),
            spell('Shield', 113, armor=7, rounds=6),
            spell('Poison', 173, damage=3, rounds=6),
            spell('Recharge', 229, mana=101, rounds=5)]

    @time_it
    def part1(self, input_data):
        wins = play_game(self.player_hp, self.player_mana, self.boss_hp,
                         self.boss_dmg, self.spells)
        return min(d for d, _ in wins), len(wins)

    @time_it
    def part2(self, input_data):
        wins = play_game(self.player_hp, self.player_mana, self.boss_hp,
                         self.boss_dmg, self.spells, hard_mode=True)
        return min(d for d, _ in wins), len(wins)


def play_game(player_hp: int, player_mana: int,
              boss_hp: int, boss_damage: int,
              spells: list['spell'], hard_mode: bool = False):
    queue = [(0, player_hp, player_mana,
              dict[tuple[int, 'spell']](), boss_hp,
              True, [])]
    seen = set()
    wins = []

    while queue:
        (spent, player_hp, player_mana, effects,
         boss_hp, player_turn, path) = queue.pop()

        key = (player_hp, player_mana, boss_hp, str(effects))
        if key in seen:
            continue
        seen.add(key)

        if hard_mode and not player_turn:
            player_hp -= 1

            if player_hp <= 0:
                continue

        # effects
        armor = sum(e.armor for _, e in effects)
        boss_hp -= sum(e.damage for _, e in effects)
        player_hp += sum(e.health for _, e in effects)
        player_mana += sum(e.mana for _, e in effects)
        effects = [(d - 1, e) for d, e in effects if d > 1]

        if boss_hp <= 0:
            wins.append((spent, path))
            continue

        if player_turn:
            for effect in spells:
                # cast chosen spell
                if (player_mana < effect.cost or
                        effect in (e for _, e in effects) or
                        spent + effect.cost >=
                        min((d for d, _ in wins), default=float('inf'))):
                    continue

                queue.append((spent + effect.cost, player_hp,
                              player_mana - effect.cost,
                              effects.copy() + [effect.cast()], boss_hp,
                              not player_turn, path + [effect.name]))
        else:
            # boss turn
            player_hp -= max(1, boss_damage - armor)

            if player_hp > 0:
                queue.append((spent, player_hp, player_mana, effects,
                              boss_hp, not player_turn, path))

    return wins


@dataclass(frozen=True)
class spell:
    name: str
    cost: int
    damage: int = 0
    health: int = 0
    armor: int = 0
    mana: int = 0
    rounds: int = 0

    def cast(self):
        return (self.rounds, self)


if __name__ == "__main__":
    DaySolution(day_number).run()
