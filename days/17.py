from days import aoc, day
from utils.decorators import time_it
from itertools import combinations

day_number = 17


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.containers = [int(i) for i in input_data]
        self.eggnog = 150

    @time_it
    def part1(self, input_data):
        combos = [p for i in range(2, len(self.containers))
                  for p in combinations(self.containers, i)
                  if sum(p) == self.eggnog]

        return len(combos)

    @time_it
    def part2(self, input_data):
        combos = [p for i in range(2, len(self.containers))
                  for p in combinations(self.containers, i)
                  if sum(p) == self.eggnog]

        lengths = [len(p) for p in combos]
        min_ = min(lengths)

        return min_, lengths.count(min_)


if __name__ == "__main__":
    DaySolution(day_number).run()
