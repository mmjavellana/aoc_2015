from days import aoc, day

day_number = 2


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        pass

    def part1(self, input_data):
        area = 0

        for x in input_data:
            (l, w, h) = map(int, x.split('x'))
            area += 2*l*w + 2*w*h + 2*l*h + min(l*w, w*h, h*l)

        return area

    def part2(self, input_data):
        length = 0

        for x in input_data:
            (l, w, h) = map(int, x.split('x'))
            bow = l * w * h

            length += min(2*l+2*w, 2*w+2*h, 2*h+2*l) + bow

        return length


if __name__ == "__main__":
    DaySolution(day_number).run()
