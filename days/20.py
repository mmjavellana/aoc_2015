from days import aoc, day
from utils.decorators import time_it

day_number = 20


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.target = int(input_data[0])

    @time_it
    def part1(self, input_data):
        target = self.target // 10
        houses = [0 for _ in range(target + 1)]

        for elf in range(1, target):
            for house in range(elf, target, elf):
                houses[house] += elf

        house = min(i for i, n in enumerate(houses) if n >= target)
        return house, houses[house] * 10

    @time_it
    def part2(self, input_data):
        for i in range(1, self.target):
            gifts = 11 * sum(i // n for n in range(1, 50) if not i % n)

            if gifts >= self.target:
                return i, gifts


def get_factors(num: int):
    return {factor
            for n in range(1, int(num**0.5) + 1) if not num % n
            for factor in (n, num//n)}


if __name__ == "__main__":
    DaySolution(day_number).run()
