from days import aoc, day
from utils.decorators import time_it

day_number = 23


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.commands = [(i + ' ').replace(',', '').split(' ', 2)
                         for i in input_data]

    @time_it
    def part1(self, input_data):
        return run_program(self.commands)

    @time_it
    def part2(self, input_data):
        return run_program(self.commands, {'a': 1, 'b': 0})


def run_program(commands: list, registers: dict = {'a': 0, 'b': 0}) -> dict:
    pointer = 0

    while pointer < len(commands):
        a, b, c = commands[pointer]

        match a:
            case 'hlf': registers[b] //= 2
            case 'tpl': registers[b] *= 3
            case 'inc': registers[b] += 1
            case 'jmp':
                pointer += int(b)
                continue
            case 'jie':
                if not registers[b] % 2:
                    pointer += int(c)
                    continue
            case 'jio':
                if registers[b] == 1:
                    pointer += int(c)
                    continue

        pointer += 1

    return registers


if __name__ == "__main__":
    DaySolution(day_number).run()
