from os import makedirs, path
from glob import glob

import requests

from aocdays import AOCDays

modules = filter(lambda x: not path.basename(x).startswith("_"),
                 glob(path.dirname(__file__) + "/*.py"))
__all__ = [path.basename(f)[:-3] for f in modules]


def day(day_number):
    def day_decorator(cls):
        if not str(cls.__module__).replace("days.", "").startswith("_"):
            AOCDays.get_instance().add_day(day_number, cls)
        return cls
    return day_decorator


class aoc:
    year = 2015
    day_number = 0
    session_token = ""
    input_filename = ""
    input_data = None

    def __init__(self, day_number, year=2015, session_token=None):
        self.year = year
        self.day_number = day_number
        self.session_token = session_token
        self.input_filename = (path.abspath(path.join(path.dirname(__file__),
                               "..", f"inputs/{day_number:02}.txt")))
        self.output_filename = (path.abspath(path.join(path.dirname(__file__),
                                "..", f"outputs/{day_number:02}.txt")))

        makedirs(path.dirname(self.input_filename), exist_ok=True)
        makedirs(path.dirname(self.output_filename), exist_ok=True)

    def download_input(self):
        if path.isfile(self.input_filename):
            return

        print("Could not find input data for day {self.day_number}"
              ", please wait while I download it...")

        input_url = ("https://adventofcode.com/{}/day/{}/input"
                     .format(self.year, self.day_number))
        result = requests.get(input_url,
                              cookies={'session': self.session_token})
        if result.status_code == 200:
            self.input_data = result.text
            with open(self.input_filename, 'w') as f:
                f.write(result.text)
        else:
            raise ConnectionError(
                    "Could not connect to AoC website to download input data. "
                    "Error code {}: {}"
                    .format(result.status_code, result.text))

    def load_input(self):
        if self.input_filename:
            with open(self.input_filename, 'r') as f:
                self.input_data = [x.strip() for x in f.readlines()]

    def run(self):
        self.download_input()
        self.load_input()

        input_data = self.input_data

        print(f"Day {self.day_number}")

        self.common(input_data)
        print(f"Part 1: {self.part1(input_data)}")
        print(f"Part 2: {self.part2(input_data)}")

    def common(self, input_data):
        pass

    def part1(self, input_data):
        pass

    def part2(self, input_data):
        pass
