from days import aoc, day
from utils.decorators import time_it
from itertools import combinations
from math import prod

day_number = 24


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.weights = [int(i) for i in input_data]

    @time_it
    def part1(self, input_data):
        return get_qe_simple(self.weights, 3)

    @time_it
    def part2(self, input_data):
        return get_qe_simple(self.weights, 4)


def get_qe_simple(array, bins):
    target = sum(array) / bins

    for i in range(1, len(array)):
        sets = [c for c in combinations(array, i) if sum(c) == target]
        if sets:
            return min(prod(c) for c in sets)


def get_qe(array, bins):
    target = sum(array) / bins
    combos = [list(zip(*s)) for s in subset_sum(array, target)]
    combos.sort(key=lambda x: len(x[0]))

    def recurse(new_combos, new_bins):
        if new_bins == 0:
            return True
        else:
            for indexes in new_combos:
                filtered_combos = [i for i in new_combos
                                   if not set(indexes).intersection(i)]

                if recurse(filtered_combos, new_bins - 1):
                    return new_combos

    sets = []
    min_length = 1
    for indexes, combo in combos:
        if sets and len(combo) > min_length:
            break

        n_c = [(i, c) for i, c in combos if not set(indexes).intersection(i)]

        if recurse(n_c, bins - 1):
            min_length = len(combo)
            sets.append(combo)

    return len(combos), len(sets), min(prod(s) for s in sets)


def subset_sum(array, target):
    array = sorted(array)
    dp = {0: [-1]}

    for i in range(len(array)):
        for k in list(dp):
            s = array[i] + k
            if s > target:
                continue
            dp.setdefault(s, []).append(i)

    def recurse(new_target, max_i):
        for i in dp[new_target]:
            if i < 0:
                yield []
            elif max_i <= i:
                break
            else:
                for answer in recurse(new_target - array[i], i):
                    answer.append((i, array[i]))
                    yield answer

    for answer in recurse(target, len(array)):
        yield answer


if __name__ == "__main__":
    DaySolution(day_number).run()
