from days import aoc, day

day_number = 8


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        pass

    def part1(self, input_data):
        return (sum([len(s) for s in input_data])
                - sum([len(bytes(s[1: -1], "utf-8")
                           .decode("unicode_escape")) for s in input_data]))

    def part2(self, input_data):
        return (sum([len('"' + s.encode("unicode_escape")
                    .decode('utf-8').replace('"', r'\"') + '"') 
                    for s in input_data])
                - sum([len(s) for s in input_data]))


if __name__ == "__main__":
    DaySolution(day_number).run()
