from days import aoc, day
from utils.decorators import time_it
from math import ceil

day_number = 21


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.boss = tuple(int(i.split()[-1]) for i in input_data)

        self.weapons = {'Dagger': (8, 4, 0),
                        'Shortsword': (10, 5, 0),
                        'Warhammer': (25, 6, 0),
                        'Longsword': (40, 7, 0),
                        'Greataxe': (74, 8, 0)}

        self.armor = {'None': (0, 0, 0),
                      'Leather': (13, 0, 1),
                      'Chainmail': (31, 0, 2),
                      'Splintmail': (53, 0, 3),
                      'Bandedmail': (75, 0, 4),
                      'Platemail': (102, 0, 5)}

        self.rings = {'None 1': (0, 0, 0),
                      'None 2': (0, 0, 0),
                      'Damage +1': (25, 1, 0),
                      'Damage +2': (50, 2, 0),
                      'Damage +3': (100, 3, 0),
                      'Defense +1': (20, 0, 1),
                      'Defense +2': (40, 0, 2),
                      'Defense +3': (80, 0, 3)}

        self.equip = [(w, a, r1, r2)
                      for w in self.weapons
                      for a in self.armor
                      for r1 in self.rings
                      for r2 in self.rings if r2 != r1]

        self.scores = [tuple(map(sum,
                             zip(*(self.weapons[w],
                                   self.armor[a],
                                   self.rings[r1],
                                   self.rings[r2]))))
                       for w, a, r1, r2 in self.equip]

    @time_it
    def part1(self, input_data):
        return min(c for c, attack, defense in self.scores
                   if (ceil(self.boss[0] / max(1, attack - self.boss[2])) <=
                       ceil(100 / max(1, self.boss[1] - defense))))

    @time_it
    def part2(self, input_data):
        return max(c for c, attack, defense in self.scores
                   if (ceil(self.boss[0] / max(1, attack - self.boss[2])) >
                       ceil(100 / max(1, self.boss[1] - defense))))


if __name__ == "__main__":
    DaySolution(day_number).run()
