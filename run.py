#!/usr/bin/env python
import datetime
import json
import sys
import shutil
import traceback
import os

if __name__ == "__main__":
    run_everything = False
    if len(sys.argv) >= 2:
        try:
            if sys.argv[1] == "all":
                run_everything = True
                day_number = 1
            else:
                day_number = int(sys.argv[1])
        except Exception:
            print(f"{sys.argv[0]} - Runs a given AOC day"
                  " (or the current day if no arguments are given)")
            print(f"Usage: {sys.argv[0]} [day]")
            sys.exit(1)
    else:
        day_number = datetime.date.today().day

    if day_number <= 0 or day_number > 25:
        print("Day given is out of rante 1 - 25")
        sys.exit(2)

    if os.path.isfile("settings.json"):
        with open("settings.json", 'r') as s:
            settings = json.loads("".join(s.readlines()))
    else:
        shutil.copy("settings.json.default", "settings.json")
        print("Please fill in settings.json first!")
        sys.exit(3)

    session_token = settings['session_token']
    year = settings['year']

    days_to_run = [day_number]
    if run_everything:
        days_to_run = [x + 1 for x in range(25)]

    # Dynamically try to load the requested day(s)
    from days import *  # noqa: F403,F401
    from aocdays import AOCDays
    days: AOCDays = AOCDays.get_instance()

    for d in days_to_run:
        day = days.get_day(d)

        if day:
            print(f"Attempting to run AoC day {d}")

            try:
                instance = day(d, year, session_token)
                instance.run()
            except ConnectionError as e:
                print(e, file=sys.stderr)
            except Exception:
                traceback.print_exc()
        else:
            if (d == (datetime.date.today().day)
                    or year < datetime.date.today().year):
                # Create today's files
                template_filename = os.path.join(os.path.dirname(__file__),
                                                 "days/_template.py")
                newday_filename = os.path.join(os.path.dirname(__file__),
                                               f"days/{d:02}.py")
                shutil.copy(template_filename, newday_filename)
                with open(newday_filename, 'r') as f:
                    lines = f.readlines()
                lines = [x.replace("day_number = 0", f"day_number = {d}")
                         for x in lines]

                with open(newday_filename, 'w') as f:
                    f.writelines(lines)
            else:
                print(f"Attempting to run AoC day {d}...")
                print(f"I have nothing to run for day {d}")
