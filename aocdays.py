class AOCDays:
    _instance = None
    days = None

    def __init__(self):
        self.days = {i: None for i in range(26)}

    def add_day(self, number: int, cls: 'days.aoc') -> None:
        self.days[number] = cls

    def get_day(self, number: int) -> 'days.aoc':
        return self.days[number]

    @classmethod
    def get_instance(cls) -> 'AOCDays':
        if not cls._instance:
            cls._instance = AOCDays()
        return cls._instance
